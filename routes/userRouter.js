const express = require("express");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const {
  signupUser,
  loginUser,
  getUsers,
  getUserById,
  deleteUser,
  updateUser,
  blockUser,
  unblockUser,
  handleRefreshToken,
  logoutUser,
  updatePassword,
  createForgotPasswordToken,
  resetPassword,
  loginAdmin,
  getWishlist,
  saveAddress,
  addToCart,
  getCart,
  emptyCart,
  applyCoupon,
  createOrder,
  getOrders,
  updateOrderStatus,
} = require("../controllers/userController");

const router = express.Router();
router.post("/signup", signupUser);
router.post("/login", loginUser);
router.get("/logout", logoutUser);

router.post("/admin/login", loginAdmin);

router.get("/cart", authMiddleware, getCart);
router.post("/cart", authMiddleware, addToCart);
router.delete("/cart", authMiddleware, emptyCart);
router.post("/cart/coupon", authMiddleware, applyCoupon);

router.get("/order", authMiddleware, getOrders);
router.post("/order", authMiddleware, createOrder);
router.put("/order-status/:id", authMiddleware, isAdmin, updateOrderStatus);

router.put("/password", authMiddleware, updatePassword);
router.post("/forgot-password", createForgotPasswordToken);
router.put("/reset-password/:token", resetPassword);

router.get("/", getUsers);
router.get("/refresh", handleRefreshToken);

router.get("/wishlist", authMiddleware, getWishlist);
router.put("/address", authMiddleware, saveAddress);

router.get("/:id", authMiddleware, isAdmin, getUserById);
router.delete("/:id", deleteUser);
router.put("/:id", authMiddleware, updateUser);

router.put("/block/:id", authMiddleware, isAdmin, blockUser);
router.put("/unblock/:id", authMiddleware, isAdmin, unblockUser);

module.exports = router;
