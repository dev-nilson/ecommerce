const express = require("express");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const {
  createProductCategory,
  updateProductCategory,
  deleteProductCategory,
  getProductCategoryById,
  getProductCategories,
} = require("../controllers/productCategoryController");

const router = express.Router();
router.get("/:id", getProductCategoryById);
router.get("/", getProductCategories);
router.post("/", authMiddleware, isAdmin, createProductCategory);
router.put("/:id", authMiddleware, isAdmin, updateProductCategory);
router.delete("/:id", authMiddleware, isAdmin, deleteProductCategory);

module.exports = router;
