const express = require("express");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const {
  createBlog,
  updateBlog,
  getBlogById,
  getBlogs,
  deleteBlog,
  likeBlog,
  dislikeBlog,
  uploadImages,
} = require("../controllers/blogController");
const { uploadImage, resizeBlogImage } = require("../middlewares/uploadImages");

const router = express.Router();

router.get("/", getBlogs);
router.get("/:id", getBlogById);
router.put(
  "/upload/:id",
  authMiddleware,
  isAdmin,
  uploadImage.array("images", 2),
  resizeBlogImage,
  uploadImages
);
router.put("/like", authMiddleware, likeBlog);
router.put("/dislike", authMiddleware, dislikeBlog);
router.post("/", authMiddleware, isAdmin, createBlog);
router.put("/:id", authMiddleware, isAdmin, updateBlog);
router.delete("/:id", authMiddleware, isAdmin, deleteBlog);

module.exports = router;
