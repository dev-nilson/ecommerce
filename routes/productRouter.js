const express = require("express");
const { authMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const {
  createProduct,
  getProductById,
  getProducts,
  updateProduct,
  deleteProduct,
  addToWishList,
  getRatings,
  uploadImages,
} = require("../controllers/productController");
const {
  uploadImage,
  resizeProductImage,
} = require("../middlewares/uploadImages");

const router = express.Router();

router.get("/", getProducts);
router.get("/:id", getProductById);
router.put(
  "/upload/:id",
  authMiddleware,
  isAdmin,
  uploadImage.array("images", 10),
  resizeProductImage,
  uploadImages
);
router.put("/ratings", authMiddleware, getRatings);
router.put("/wishlist", authMiddleware, addToWishList);
router.post("/", authMiddleware, isAdmin, createProduct);
router.put("/:id", authMiddleware, isAdmin, updateProduct);
router.delete("/:id", authMiddleware, isAdmin, deleteProduct);

module.exports = router;
