const slugify = require("slugify");
const fs = require("fs");
const User = require("../models/userModel");
const Product = require("../models/productModel");
const asyncHandler = require("express-async-handler");
const validateId = require("../utils/validateId");
const cloudinaryUpload = require("../utils/cloudinary");

const createProduct = asyncHandler(async (req, res) => {
  try {
    if (req.body.title) {
      req.body.slug = slugify(req.body.title);
    }
    const newProduct = await Product.create(req.body);
    res.json(newProduct);
  } catch (error) {
    throw new Error(error);
  }
});

const getProductById = asyncHandler(async (req, res) => {
  const { id } = req.params;

  try {
    const foundProduct = await Product.findById(id);
    res.json(foundProduct);
  } catch (error) {
    throw new Error(error);
  }
});

const getProducts = asyncHandler(async (req, res) => {
  try {
    const query = { ...req.query };
    const excludedFields = ["page", "sort", "limit", "fields"];
    excludedFields.forEach((element) => delete query[element]);
    let queryString = JSON.stringify(query);

    queryString = queryString.replace(
      /\b(gte|gt|lte|lt)\b/g,
      (match) => `$${match}`
    );
    let products = Product.find(JSON.parse(queryString));

    if (req.query.sort) {
      const sortBy = req.query.sort.split(",").join(" ");
      products = products.sort(sortBy);
    } else {
      products = products.sort("-createdAt");
    }

    if (req.query.fields) {
      const fields = req.query.fields.split(",").join(" ");
      products = products.select(fields);
    } else {
      products = products.select("-__v");
    }

    const page = req.query.page;
    const limit = req.query.limit;
    const skip = (page - 1) * limit;
    products.skip(skip).limit(limit);

    if (req.query.page) {
      const total = await Product.countDocuments();
      if (skip >= total) throw new Error("This page does not exist");
    }

    products = await products;
    res.json(products);
  } catch (error) {
    throw new Error(error);
  }
});

const updateProduct = asyncHandler(async (req, res) => {
  const { id } = req.params;

  try {
    if (req.body.title) {
      req.body.slug = slugify(req.body.title);
    }

    const updatedProduct = await Product.findOneAndUpdate(
      { _id: id },
      req.body,
      {
        new: true,
      }
    );

    res.json(updatedProduct);
  } catch (error) {
    throw new Error(error);
  }
});

const deleteProduct = asyncHandler(async (req, res) => {
  const { id } = req.params;

  try {
    const deletedProduct = await Product.findByIdAndDelete(id);
    res.json(deletedProduct);
  } catch (error) {
    throw new Error(error);
  }
});

const addToWishList = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { productId } = req.body;

  try {
    const user = await User.findById(_id);
    const alreadyAdded = user.wishlist.find(
      (id) => id.toString() === productId
    );

    if (alreadyAdded) {
      const updatedUser = await User.findByIdAndUpdate(
        _id,
        {
          $pull: { wishlist: productId },
        },
        { new: true }
      );

      res.json(updatedUser);
    } else {
      const updatedUser = await User.findByIdAndUpdate(
        _id,
        {
          $push: { wishlist: productId },
        },
        { new: true }
      );

      res.json(updatedUser);
    }
  } catch (error) {
    throw new Error(error);
  }
});

const getRatings = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { stars, productId, comment } = req.body;

  try {
    const product = await Product.findById(productId);
    let alreadyRated = product.ratings.find(
      (userId) => userId.postedBy._id.toString() === _id.toString()
    );

    if (alreadyRated) {
      await Product.updateOne(
        { ratings: { $elemMatch: alreadyRated } },
        { $set: { "ratings.$.stars": stars, "ratings.$.comment": comment } },
        { new: true }
      );
    } else {
      await Product.findByIdAndUpdate(
        productId,
        {
          $push: {
            ratings: {
              stars,
              comment,
              postedBy: _id,
            },
          },
        },
        { new: true }
      );
    }

    const allRatings = await Product.findById(productId);
    let sumRatings = allRatings.ratings
      .map((item) => item.stars)
      .reduce((prev, curr) => prev + curr, 0);
    let totalRating = sumRatings / allRatings.ratings.length;
    let updatedProduct = await Product.findByIdAndUpdate(
      productId,
      { totalRating },
      { new: true }
    );

    res.json(updatedProduct);
  } catch (error) {
    throw new Error(error);
  }
});

const uploadImages = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const urls = [];
    const files = req.files;
    const uploader = (path) => cloudinaryUpload(path, "images");

    for (const file of files) {
      const { path } = file;
      const newPath = await uploader(path);
      urls.push(newPath);
      fs.unlinkSync(path);
    }

    const updatedProduct = await Product.findByIdAndUpdate(
      id,
      {
        images: urls,
      },
      { new: true }
    );

    res.json(updatedProduct);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createProduct,
  getProductById,
  getProducts,
  updateProduct,
  deleteProduct,
  addToWishList,
  getRatings,
  uploadImages,
};
