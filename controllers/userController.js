const jwt = require("jsonwebtoken");
const uniqid = require("uniqid");
const User = require("../models/userModel");
const Product = require("../models/productModel");
const Coupon = require("../models/couponModel");
const Cart = require("../models/cartModel");
const Order = require("../models/orderModel");
const validateId = require("../utils/validateId");
const { generateToken, generateRefreshToken } = require("../config/jwt");
const asyncHandler = require("express-async-handler");
const { sendEmail } = require("./emailController");

const signupUser = asyncHandler(async (req, res) => {
  const email = req.body.email;
  const findUser = await User.findOne({ email });

  if (!findUser) {
    const newUser = await User.create(req.body);
    res.json(newUser);
  } else {
    throw new Error("User already exists");
  }
});

const loginUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  const foundUser = await User.findOne({ email });

  if (foundUser && (await foundUser.isPasswordMatched(password))) {
    const refreshToken = generateRefreshToken(foundUser?._id);
    const updatedUser = await User.findByIdAndUpdate(
      foundUser?._id,
      {
        refreshToken: refreshToken,
      },
      { new: true }
    );

    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      maxAge: 72 * 60 * 60 * 1000,
    });

    res.json({
      _id: foundUser?._id,
      firstName: foundUser?.firstName,
      lastName: foundUser?.lastName,
      email: foundUser?.email,
      token: generateToken(foundUser?._id),
    });
  } else {
    throw new Error("Invalid credentials");
  }
});

const loginAdmin = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  const foundAdmin = await User.findOne({ email });

  if (foundAdmin.role !== "admin") {
    throw new Error("Not authorized");
  }

  if (foundAdmin && (await foundAdmin.isPasswordMatched(password))) {
    const refreshToken = generateRefreshToken(foundAdmin?._id);
    const updatedUser = await User.findByIdAndUpdate(
      foundAdmin?._id,
      {
        refreshToken: refreshToken,
      },
      { new: true }
    );

    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      maxAge: 72 * 60 * 60 * 1000,
    });

    res.json({
      _id: foundAdmin?._id,
      firstName: foundAdmin?.firstName,
      lastName: foundAdmin?.lastName,
      email: foundAdmin?.email,
      token: generateToken(foundAdmin?._id),
    });
  } else {
    throw new Error("Invalid credentials");
  }
});

const logoutUser = asyncHandler(async (req, res) => {
  const cookie = req.cookies;

  if (!cookie) {
    throw new Error("No refresh token found");
  }

  const refreshToken = cookie.refreshToken;
  const user = await User.findOne({ refreshToken });

  res.clearCookie("refreshToken", {
    httpOnly: true,
    secure: true,
  });

  await User.findOneAndUpdate(refreshToken, {
    refreshToken: "",
  });

  return res.sendStatus(204);
});

const getUsers = asyncHandler(async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (error) {
    throw new Error(error);
  }
});

const getUserById = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const user = await User.findById(id);
    res.json(user);
  } catch (error) {
    throw new Error(error);
  }
});

const deleteUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const user = await User.findByIdAndDelete(id);
    res.json(user);
  } catch (error) {
    throw new Error(error);
  }
});

const updateUser = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateId(_id);

  try {
    const user = await User.findByIdAndUpdate(
      _id,
      {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
      },
      { new: true }
    );
    res.json(user);
  } catch (error) {
    throw new Error(error);
  }
});

const blockUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const user = await User.findByIdAndUpdate(
      id,
      {
        isBlocked: true,
      },
      { new: true }
    );
    res.json({ message: "Blocked user" });
  } catch (error) {
    throw new Error(error);
  }
});

const unblockUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const user = await User.findByIdAndUpdate(
      id,
      {
        isBlocked: false,
      },
      { new: true }
    );
    res.json({ message: "Unblocked user" });
  } catch (error) {
    throw new Error(error);
  }
});

const handleRefreshToken = asyncHandler(async (req, res) => {
  const cookie = req.cookies;

  if (!cookie) {
    throw new Error("No refresh token found");
  }

  const refreshToken = cookie.refreshToken;
  const user = await User.findOne({ refreshToken });

  if (!user) {
    throw new Error("No refresh token found");
  }

  jwt.verify(refreshToken, process.env.JWT_SECRET, (error, decoded) => {
    if (error || user.id !== decoded.id) {
      throw new Error("There is something wrong");
    }

    const accessToken = generateRefreshToken(user?._id);
    res.json({ accessToken });
  });
});

const updatePassword = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { password } = req.body;
  validateId(_id);

  const user = await User.findById(_id);
  if (password) {
    user.password = password;
    const updatedPassword = await user.save();
    res.json(updatedPassword);
  } else {
    res.json(user);
  }
});

const createForgotPasswordToken = asyncHandler(async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error("User not found");
  }

  try {
    const token = await user.createPasswordResetToken();
    await user.save();
    const resetUrl = `<a href="http://localhost:5000/api/users/reset-password/${token}">Reset Password</a>`;
    const data = {
      to: email,
      subject: "Reset Password",
      text: "Hey, user",
      html: resetUrl,
    };

    sendEmail(data);
    res.json(token);
  } catch (error) {
    throw new Error(error);
  }
});

const resetPassword = asyncHandler(async (req, res) => {
  const { password } = req.body;
  const { token } = req.params;
  const user = await User.findOne({
    passwordResetToken: token,
    passwordResetExpires: { $gt: Date.now() },
  });

  if (!user) throw new Error("Token expired.");
  user.password = password;
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;

  await user.save();
  res.json(user);
});

const getWishlist = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateId(_id);

  try {
    const foundUser = await User.findById(_id).populate("wishlist");
    res.json(foundUser);
  } catch (error) {
    throw new Error(error);
  }
});

const saveAddress = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateId(_id);

  try {
    const updatedUser = await User.findByIdAndUpdate(
      _id,
      { address: req.body.address },
      { new: true }
    );
    res.json(updatedUser);
  } catch (error) {
    throw new Error(error);
  }
});

const addToCart = asyncHandler(async (req, res) => {
  const { cart } = req.body;
  const { _id } = req.user;
  validateId(_id);

  try {
    let products = [];
    const user = await User.findById(_id);
    const hasCart = await Cart.findOne({ customer: user._id });

    if (hasCart) {
      hasCart.remove();
    }

    for (let i = 0; i < cart.length; i++) {
      let getPrice = await Product.findById(cart[i]._id).select("price").exec();

      let object = {};
      object.product = cart[i]._id;
      object.count = cart[i].count;
      object.color = cart[i].color;
      object.price = getPrice.price;

      products.push(object);
    }

    let total = 0;
    for (let i = 0; i < products.length; i++) {
      total += products[i].price * products[i].count;
    }

    let newCart = await new Cart({
      products,
      total,
      customer: user.id,
    }).save();

    res.json(newCart);
  } catch (error) {
    throw new Error(error);
  }
});

const getCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateId(_id);

  try {
    const cart = await Cart.findOne({ customer: _id }).populate(
      "products.product"
    );
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

const emptyCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateId(_id);

  try {
    const cart = await Cart.findOneAndRemove({ customer: _id });
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

const applyCoupon = asyncHandler(async (req, res) => {
  const { coupon } = req.body;
  const { _id } = req.user;
  const validCoupon = await Coupon.findOne({ name: coupon });

  if (!validCoupon) {
    throw new Error("Invalid coupon");
  }

  const user = await User.findById(_id);
  const { products, total } = await Cart.findOne({
    customer: user._id,
  }).populate("products.product");

  let finalTotal = (total - (total * validCoupon.discount) / 100).toFixed(2);

  await Cart.findOneAndUpdate(
    { customer: user._id },
    { finalTotal },
    { new: true }
  );

  res.json(finalTotal);
});

const createOrder = asyncHandler(async (req, res) => {
  const { cash, couponApplied } = req.body;
  const { _id } = req.user;
  validateId(_id);

  try {
    if (!cash) throw new Error("Order failed");

    const user = await User.findById(_id);
    let cart = await Cart.findOne({
      customer: user._id,
    });

    let orderTotal = 0;

    if (couponApplied && cart.finalTotal) {
      orderTotal = cart.finalTotal;
    } else {
      orderTotal = cart.total;
    }

    let newOrder = await new Order({
      products: cart.products,
      payment: {
        id: uniqid(),
        method: "Cash",
        amount: orderTotal,
        status: "Processing",
        created: Date.now(),
        currency: "USD",
      },
      customer: user._id,
      status: "Processing",
    }).save();

    let updateProduct = cart.products.map((item) => {
      return {
        updateOne: {
          filter: { _id: item.product._id },
          update: { $inc: { quantity: -item.count, sold: +item.count } },
        },
      };
    });

    const updatedProduct = await Product.bulkWrite(updateProduct, {});
    res.json(updatedProduct);
  } catch (error) {
    throw new Error(error);
  }
});

const getOrders = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateId(_id);

  try {
    const orders = await Order.findOne({ customer: _id }).populate(
      "products.product"
    );
    res.json(orders);
  } catch (error) {
    throw new Error(error);
  }
});

const updateOrderStatus = asyncHandler(async (req, res) => {
  const { status } = req.body;
  const { id } = req.params;
  validateId(id);

  try {
    const order = await Order.findByIdAndUpdate(id, { status }, { new: true });
    res.json(order);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  signupUser,
  loginUser,
  loginAdmin,
  getUsers,
  getUserById,
  deleteUser,
  updateUser,
  blockUser,
  unblockUser,
  handleRefreshToken,
  logoutUser,
  updatePassword,
  createForgotPasswordToken,
  resetPassword,
  getWishlist,
  saveAddress,
  addToCart,
  getCart,
  emptyCart,
  applyCoupon,
  createOrder,
  getOrders,
  updateOrderStatus,
};
