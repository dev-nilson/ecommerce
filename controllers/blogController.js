const fs = require("fs");
const Blog = require("../models/blogModel");
const validateId = require("../utils/validateId");
const asyncHandler = require("express-async-handler");
const cloudinaryUpload = require("../utils/cloudinary");

const createBlog = asyncHandler(async (req, res) => {
  try {
    const createdBlog = await Blog.create(req.body);
    res.json(createdBlog);
  } catch (error) {
    throw new Error(error);
  }
});

const updateBlog = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const updatedBlog = await Blog.findByIdAndUpdate(id, req.body, {
      new: true,
    });
    res.json(updatedBlog);
  } catch (error) {
    throw new Error(error);
  }
});

const getBlogById = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const blog = await Blog.findById(id).populate("likes").populate("dislikes");
    await Blog.findByIdAndUpdate(id, { $inc: { views: 1 } }, { new: true });
    res.json(blog);
  } catch (error) {
    throw new Error(error);
  }
});

const getBlogs = asyncHandler(async (req, res) => {
  try {
    const blogs = await Blog.find();
    res.json(blogs);
  } catch (error) {
    throw new Error(error);
  }
});

const deleteBlog = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const deletedBlog = await Blog.findByIdAndDelete(id);
    res.json(deletedBlog);
  } catch (error) {
    throw new Error(error);
  }
});

const likeBlog = asyncHandler(async (req, res) => {
  const { blogId } = req.body;
  validateId(blogId);
  const blog = await Blog.findById(blogId);
  const userId = req.user._id;
  const isLiked = blog.isLiked;
  const isDisliked = blog.dislikes.find(
    (id) => id.toString() === userId.toString()
  );

  if (isDisliked) {
    const updatedBlog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $pull: { dislikes: userId },
        isDisliked: false,
      },
      { new: true }
    );

    res.json(updatedBlog);
  }

  if (isLiked) {
    const updatedBlog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $pull: { likes: userId },
        isLiked: false,
      },
      { new: true }
    );

    res.json(updatedBlog);
  } else {
    const updatedBlog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $push: { likes: userId },
        isLiked: true,
      },
      { new: true }
    );

    res.json(updatedBlog);
  }
});

const dislikeBlog = asyncHandler(async (req, res) => {
  const { blogId } = req.body;
  validateId(blogId);
  const blog = await Blog.findById(blogId);
  const userId = req.user._id;
  const isDisliked = blog.isDisliked;
  const isLiked = blog.likes.find((id) => id.toString() === userId.toString());

  if (isLiked) {
    const updatedBlog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $pull: { likes: userId },
        isLiked: false,
      },
      { new: true }
    );

    res.json(updatedBlog);
  }

  if (isDisliked) {
    const updatedBlog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $pull: { dislikes: userId },
        isDisliked: false,
      },
      { new: true }
    );

    res.json(updatedBlog);
  } else {
    const updatedBlog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $push: { dislikes: userId },
        isDisliked: true,
      },
      { new: true }
    );

    res.json(updatedBlog);
  }
});

const uploadImages = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const urls = [];
    const files = req.files;
    const uploader = (path) => cloudinaryUpload(path, "images");

    for (const file of files) {
      const { path } = file;
      const newPath = await uploader(path);
      urls.push(newPath);
      fs.unlinkSync(path);
    }

    const updatedBlog = await Blog.findByIdAndUpdate(
      id,
      {
        images: urls,
      },
      { new: true }
    );

    res.json(updatedBlog);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createBlog,
  updateBlog,
  getBlogById,
  getBlogs,
  deleteBlog,
  likeBlog,
  dislikeBlog,
  uploadImages,
};
