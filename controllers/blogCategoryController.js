const BlogCategory = require("../models/blogCategoryModel");
const asyncHandler = require("express-async-handler");
const validateId = require("../utils/validateId");

const getBlogCategories = asyncHandler(async (req, res) => {
  try {
    const categories = await BlogCategory.find();
    res.json(categories);
  } catch (error) {
    throw new Error(error);
  }
});

const getBlogCategoryById = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);
  
  try {
    const category = await BlogCategory.findById(id);
    res.json(category);
  } catch (error) {
    throw new Error(error);
  }
});

const createBlogCategory = asyncHandler(async (req, res) => {
  try {
    const createdCategory = await BlogCategory.create(req.body);
    res.json(createdCategory);
  } catch (error) {
    throw new Error(error);
  }
});

const updateBlogCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const updatedCategory = await BlogCategory.findByIdAndUpdate(
      id,
      req.body,
      { new: true }
    );
    res.json(updatedCategory);
  } catch (error) {
    throw new Error(error);
  }
});

const deleteBlogCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const deletedCategory = await BlogCategory.findByIdAndDelete(id);
    res.json(deletedCategory);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  getBlogCategories,
  getBlogCategoryById,
  updateBlogCategory,
  createBlogCategory,
  deleteBlogCategory,
};
