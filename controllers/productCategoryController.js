const ProductCategory = require("../models/productCategoryModel");
const asyncHandler = require("express-async-handler");
const validateId = require("../utils/validateId");

const getProductCategories = asyncHandler(async (req, res) => {
  try {
    const categories = await ProductCategory.find();
    res.json(categories);
  } catch (error) {
    throw new Error(error);
  }
});

const getProductCategoryById = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);
  
  try {
    const category = await ProductCategory.findById(id);
    res.json(category);
  } catch (error) {
    throw new Error(error);
  }
});

const createProductCategory = asyncHandler(async (req, res) => {
  try {
    const createdCategory = await ProductCategory.create(req.body);
    res.json(createdCategory);
  } catch (error) {
    throw new Error(error);
  }
});

const updateProductCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const updatedCategory = await ProductCategory.findByIdAndUpdate(
      id,
      req.body,
      { new: true }
    );
    res.json(updatedCategory);
  } catch (error) {
    throw new Error(error);
  }
});

const deleteProductCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const deletedCategory = await ProductCategory.findByIdAndDelete(id);
    res.json(deletedCategory);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  getProductCategories,
  getProductCategoryById,
  updateProductCategory,
  createProductCategory,
  deleteProductCategory,
};
