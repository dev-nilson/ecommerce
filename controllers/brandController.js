const Brand = require("../models/brandModel");
const asyncHandler = require("express-async-handler");
const validateId = require("../utils/validateId");

const getBrands = asyncHandler(async (req, res) => {
  try {
    const brands = await Brand.find();
    res.json(brands);
  } catch (error) {
    throw new Error(error);
  }
});

const getBrandById = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);
  
  try {
    const brand = await Brand.findById(id);
    res.json(brand);
  } catch (error) {
    throw new Error(error);
  }
});

const createBrand = asyncHandler(async (req, res) => {
  try {
    const createdBrand = await Brand.create(req.body);
    res.json(createdBrand);
  } catch (error) {
    throw new Error(error);
  }
});

const updateBrand = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const updatedBrand = await Brand.findByIdAndUpdate(
      id,
      req.body,
      { new: true }
    );
    res.json(updatedBrand);
  } catch (error) {
    throw new Error(error);
  }
});

const deleteBrand = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateId(id);

  try {
    const deletedBrand = await Brand.findByIdAndDelete(id);
    res.json(deletedBrand);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  getBrands,
  getBrandById,
  updateBrand,
  createBrand,
  deleteBrand,
};
