const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const { notFound, errorHandler } = require("./middlewares/errorHandler");
const dotenv = require("dotenv").config();
const userRouter = require("./routes/userRouter");
const productRouter = require("./routes/productRouter");
const blogRouter = require("./routes/blogRouter");
const productCategoryRouter = require("./routes/productCategoryRouter");
const blogCategoryRouter = require("./routes/blogCategoryRouter");
const brandRouter = require("./routes/brandRouter");
const couponRouter = require("./routes/couponRouter");
const app = express();

app.use(morgan("dev"));
app.use(express.json());
app.use(cookieParser());

app.use("/api/users", userRouter);
app.use("/api/products", productRouter);
app.use("/api/blogs", blogRouter);
app.use("/api/product-categories", productCategoryRouter);
app.use("/api/blog-categories", blogCategoryRouter);
app.use("/api/brands", brandRouter);
app.use("/api/coupons", couponRouter);

app.use(notFound);
app.use(errorHandler);

mongoose.set("strictQuery", false);
mongoose
  .connect(process.env.MONGO_URI)
  .then(() => {
    app.listen(process.env.PORT_NUMBER, () => {
      console.log(`Listening on port ${process.env.PORT_NUMBER}...`);
    });
  })
  .catch((err) => {
    console.log(err);
  });
